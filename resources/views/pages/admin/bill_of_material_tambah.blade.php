@extends('layouts.layout')

@push('style')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
@endpush

@section('title', 'Bill of Material')

@section('sidebar')
    @include('sidebar.admin')
@endsection

@section('content')
{{-- AWAL MAIN CONTENT --}}
<div class="main-content">
    {{-- Breadcrumb --}}
    <div class="breadcrumb-wrapper">
        <ul class="breadcrumb">
            <li><a href=""><i class="fa fa-home fa-fw"></i></a></li>
            <li class="active-bread">Bill of Material</li>
        </ul>
    </div>
    {{-- End Breadcrumb --}}

    {{-- awal container fluid --}}
    <div class="container-fluid">
        <div class="row"> {{-- awal row --}}
            <div class="col-md-12"> {{-- awal col-md-12 --}}

                {{-- awal tabel user --}}
                <div class="panel">
                    <div class="panel-heading">
                        <h3 class="panel-title">Tambah Bill of Material</h3>
                    </div>
                    {{-- awal panel body --}}
                    <div class="panel-body">

                        <div class="form-horizontal">

                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="control-label">Customer</label>
                                        <input type="text" class="form-control" id="customer" name="customer" placeholder="Customer">

                                        <label class="control-label">Order No</label>
                                        <input type="text" class="form-control" id="order_no" name="order_no" placeholder="Order No">

                                        <label class="control-label">Article No</label>
                                        <input type="text" class="form-control" id="article_no" name="article_no" placeholder="Article No">

                                        <label class="control-label">Color</label>
                                        <input type="text" class="form-control" id="color" name="color" placeholder="Color">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <label class="control-label">PP No</label>
                                        <input type="text" class="form-control" id="pp_no" name="pp_no" placeholder="PP No">

                                        <label class="control-label">Style</label>
                                        <input type="text" class="form-control" id="style" name="style" placeholder="Style">

                                        <label class="control-label">Ex Fctry</label>
                                        <input type="text" class="form-control" id="ex_fctry" name="ex_fctry" placeholder="Ex Fctry">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                <br>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover" id="">
                                            <thead>
                                                <tr>
                                                    <th>35</th>
                                                    <th>36</th>
                                                    <th>37</th>
                                                    <th>38</th>
                                                    <th>39</th>
                                                    <th>40</th>
                                                    <th>41</th>
                                                    <th>42</th>
                                                    <th>43</th>
                                                    <th>44</th>
                                                    <th>45</th>
                                                    <th>46</th>
                                                    <th>47</th>
                                                    <th>48</th>
                                                    <th>49</th>
                                                    <th>50</th>
                                                    <th>51</th>
                                                    <th>52</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><input type="text" class="form-control" id="35" name="35" placeholder="-"></td>
                                                    <td><input type="text" class="form-control" id="36" name="36" placeholder="-"></td>
                                                    <td><input type="text" class="form-control" id="37" name="37" placeholder="-"></td>
                                                    <td><input type="text" class="form-control" id="38" name="38" placeholder="-"></td>
                                                    <td><input type="text" class="form-control" id="39" name="39" placeholder="-"></td>
                                                    <td><input type="text" class="form-control" id="40" name="40" placeholder="-"></td>
                                                    <td><input type="text" class="form-control" id="41" name="41" placeholder="-"></td>
                                                    <td><input type="text" class="form-control" id="42" name="42" placeholder="-"></td>
                                                    <td><input type="text" class="form-control" id="43" name="43" placeholder="-"></td>
                                                    <td><input type="text" class="form-control" id="44" name="44" placeholder="-"></td>
                                                    <td><input type="text" class="form-control" id="45" name="45" placeholder="-"></td>
                                                    <td><input type="text" class="form-control" id="46" name="46" placeholder="-"></td>
                                                    <td><input type="text" class="form-control" id="47" name="47" placeholder="-"></td>
                                                    <td><input type="text" class="form-control" id="48" name="48" placeholder="-"></td>
                                                    <td><input type="text" class="form-control" id="49" name="49" placeholder="-"></td>
                                                    <td><input type="text" class="form-control" id="50" name="50" placeholder="-"></td>
                                                    <td><input type="text" class="form-control" id="51" name="51" placeholder="-"></td>
                                                    <td><input type="text" class="form-control" id="52" name="52" placeholder="-"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <!-- REPEATER -->
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <!-- <form name="add_name" id="add_name"> -->
                                        <div class="table-responsive">
                                        <!-- AWAL CUTTING -->
                                        <h4><b>CUTTING</b></h4>
                                            <table class="table table-bordered table-hover" id="dynamic_field_cut">
                                                <thead>
                                                    <tr>
                                                        <th>Material</th>
                                                        <th>Usage</th>
                                                        <th>Article</th>
                                                        <th>Col/Size</th>
                                                        <th>Consmpt</th>
                                                        <th>UOM</th>
                                                        <th>Needed</th>
                                                        <th>Remarks</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><input type="text" class="form-control" id="cut_material" name="cut_material" placeholder="Material"></td>
                                                        <td><input type="text" class="form-control" id="cut_usage" name="cut_usage" placeholder="Usage"></td>
                                                        <td><input type="text" class="form-control" id="cut_article" name="cut_article" placeholder="Article"></td>
                                                        <td><input type="text" class="form-control" id="cut_colsize" name="cut_colsize" placeholder="Col/Size"></td>
                                                        <td><input type="text" class="form-control" id="cut_consmpt" name="cut_consmpt" placeholder="Consmpt"></td>
                                                        <td><input type="text" class="form-control" id="cut_uom" name="cut_uom" placeholder="UOM"></td>
                                                        <td><input type="text" class="form-control" id="cut_needed" name="cut_needed" placeholder="Needed"></td>
                                                        <td><input type="text" class="form-control" id="cut_remarks" name="cut_remarks" placeholder="Remarks"></td>
                                                        <td><button type="button" name="add_cut" id="add_cut" class="btn btn-success">Add</button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!-- AKHIR CUTTING -->

                                            <!-- AWAL SEWING -->
                                            <h4><b>SEWING</b></h4>
                                            <table class="table table-bordered table-hover" id="dynamic_field_sew">
                                                <thead>
                                                    <tr>
                                                        <th>Material</th>
                                                        <th>Usage</th>
                                                        <th>Article</th>
                                                        <th>Col/Size</th>
                                                        <th>Consmpt</th>
                                                        <th>UOM</th>
                                                        <th>Needed</th>
                                                        <th>Remarks</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><input type="text" class="form-control" id="sew_material" name="sew_material" placeholder="Material"></td>
                                                        <td><input type="text" class="form-control" id="sew_usage" name="sew_usage" placeholder="Usage"></td>
                                                        <td><input type="text" class="form-control" id="sew_article" name="sew_article" placeholder="Article"></td>
                                                        <td><input type="text" class="form-control" id="sew_colsize" name="sew_colsize" placeholder="Col/Size"></td>
                                                        <td><input type="text" class="form-control" id="sew_consmpt" name="sew_consmpt" placeholder="Consmpt"></td>
                                                        <td><input type="text" class="form-control" id="sew_uom" name="sew_uom" placeholder="UOM"></td>
                                                        <td><input type="text" class="form-control" id="sew_needed" name="sew_needed" placeholder="Needed"></td>
                                                        <td><input type="text" class="form-control" id="sew_remarks" name="sew_remarks" placeholder="Remarks"></td>
                                                        <td><button type="button" name="add_sew" id="add_sew" class="btn btn-success">Add</button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!-- AKHIR SEWING -->

                                            <!-- AWAL PACKING -->
                                            <h4><b>PACKING</b></h4>
                                            <table class="table table-bordered table-hover" id="dynamic_field_pac">
                                                <thead>
                                                    <tr>
                                                        <th>Material</th>
                                                        <th>Usage</th>
                                                        <th>Article</th>
                                                        <th>Col/Size</th>
                                                        <th>Consmpt</th>
                                                        <th>UOM</th>
                                                        <th>Needed</th>
                                                        <th>Remarks</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td><input type="text" class="form-control" id="pac_material" name="pac_material" placeholder="Material"></td>
                                                        <td><input type="text" class="form-control" id="pac_usage" name="pac_usage" placeholder="Usage"></td>
                                                        <td><input type="text" class="form-control" id="pac_article" name="pac_article" placeholder="Article"></td>
                                                        <td><input type="text" class="form-control" id="pac_colsize" name="pac_colsize" placeholder="Col/Size"></td>
                                                        <td><input type="text" class="form-control" id="pac_consmpt" name="pac_consmpt" placeholder="Consmpt"></td>
                                                        <td><input type="text" class="form-control" id="pac_uom" name="pac_uom" placeholder="UOM"></td>
                                                        <td><input type="text" class="form-control" id="pac_needed" name="pac_needed" placeholder="Needed"></td>
                                                        <td><input type="text" class="form-control" id="pac_remarks" name="pac_remarks" placeholder="Remarks"></td>
                                                        <td><button type="button" name="add_pac" id="add_pac" class="btn btn-success">Add</button></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <!-- AKHIR PACKING -->
                                            <input type="button" id="btn-simpan" class="btn btn-primary" value="Simpan" />
                                            <a class="btn btn-danger" href="{{ url('/bill_of_material') }}"><i class="fa fa-home"></i> Batal</a>
                                        </div>
                                    <!-- </form> -->
                                </div>
                            </div>
                            <!-- AKHIR REPEATER -->

                        </div>

                    </div> {{-- akhir panel body --}}
                </div> {{-- akhir tabel user --}}

            </div> {{-- akhir col-md-12 --}}
        </div> {{-- akhir row --}}
    </div> {{-- akhir container fluid --}}

</div>
{{-- AKHIR MAIN CONTENT --}}
@endsection

@push('script')

<script>
$(document).ready(function(){
    var i=1;
    $('#add_cut').click(function(){
        i++;
        $('#dynamic_field_cut').append('<tr id="row'+i+'"><td><input type="text" class="form-control" id="cut_material" name="cut_material" placeholder="Material"></td><td><input type="text" class="form-control" id="cut_usage" name="cut_usage" placeholder="Usage"></td><td><input type="text" class="form-control" id="cut_article" name="cut_article" placeholder="Article"></td><td><input type="text" class="form-control" id="cut_colsize" name="cut_colsize" placeholder="Col/Size"></td><td><input type="text" class="form-control" id="cut_consmpt" name="cut_consmpt" placeholder="Consmpt"></td><td><input type="text" class="form-control" id="cut_uom" name="cut_uom" placeholder="UOM"></td><td><input type="text" class="form-control" id="cut_needed" name="cut_needed" placeholder="Needed"></td><td><input type="text" class="form-control" id="cut_remarks" name="cut_remarks" placeholder="Remarks"></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
    });

    $('#add_sew').click(function(){
        i++;
        $('#dynamic_field_sew').append('<tr id="row'+i+'"><td><input type="text" class="form-control" id="sew_material" name="sew_material" placeholder="Material"></td><td><input type="text" class="form-control" id="sew_usage" name="sew_usage" placeholder="Usage"></td><td><input type="text" class="form-control" id="sew_article" name="sew_article" placeholder="Article"></td><td><input type="text" class="form-control" id="sew_colsize" name="sew_colsize" placeholder="Col/Size"></td><td><input type="text" class="form-control" id="sew_consmpt" name="sew_consmpt" placeholder="Consmpt"></td><td><input type="text" class="form-control" id="sew_uom" name="sew_uom" placeholder="UOM"></td><td><input type="text" class="form-control" id="sew_needed" name="sew_needed" placeholder="Needed"></td><td><input type="text" class="form-control" id="sew_remarks" name="sew_remarks" placeholder="Remarks"></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
    });

    $('#add_pac').click(function(){
        i++;
        $('#dynamic_field_pac').append('<tr id="row'+i+'"><td><input type="text" class="form-control" id="pac_material" name="pac_material" placeholder="Material"></td><td><input type="text" class="form-control" id="pac_usage" name="pac_usage" placeholder="Usage"></td><td><input type="text" class="form-control" id="pac_article" name="pac_article" placeholder="Article"></td><td><input type="text" class="form-control" id="pac_colsize" name="pac_colsize" placeholder="Col/Size"></td><td><input type="text" class="form-control" id="pac_consmpt" name="pac_consmpt" placeholder="Consmpt"></td><td><input type="text" class="form-control" id="pac_uom" name="pac_uom" placeholder="UOM"></td><td><input type="text" class="form-control" id="pac_needed" name="pac_needed" placeholder="Needed"></td><td><input type="text" class="form-control" id="pac_remarks" name="pac_remarks" placeholder="Remarks"></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
    });
    
    $(document).on('click', '.btn_remove', function(){
        var button_id = $(this).attr("id"); 
        $('#row'+button_id+'').remove();
    });
    
    $('#submit').click(function(){      
        $.ajax({
            url:"name.php",
            method:"POST",
            data:$('#add_name').serialize(),
            success:function(data)
            {
                alert(data);
                $('#add_name')[0].reset();
            }
        });
    });
    
});
</script>

<script>
    // SIMPAN
    $("#btn-simpan").click(function(){
            swal({
            title: "Apakah Anda Yakin ?",
            text: "Data Bill of Material Ini Akan Disimpan ",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#00a65a",
            confirmButtonText: "Ya, Yakin !",
            cancelButtonText: "Tidak, Batalkan !",
            closeOnConfirm: false,
            closeOnCancel: false,
            showLoaderOnConfirm: true
        },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    url : "/",
                    type : "POST",
                    data : {
                        // "_token": "{{ csrf_token() }}",
                        // "kode_negara" : $("#tambah_kode_negara").val(),
                        // "nama_negara" : $("#tambah_nama_negara").val()
                    },
                    success : function(data, status){
                        if(status=="success"){
                            setTimeout(function(){
                                swal({
                                    title: "Sukses",
                                    text: "Data Tersimpan!",
                                    type: "success"
                                    },
                                    function(){
                                        table.ajax.reload();
                                    });
                                }, 1000);
                        }
                        // $('#modal-tambah').modal('hide');
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        setTimeout(function(){
                            swal("Gagal", "Data Gagal Disimpan", "error");
                        }, 1000);
                    }
                });
            } else {
            swal('Dibatalkan', 'Data Bill of Material Batal Simpan', 'error');
            $('#modal-tambah').modal('hide');
            }
        });
    });
</script>

@endpush