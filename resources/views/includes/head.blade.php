<title>@yield('title') | IT-INVENTORY</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<!-- vendor css -->
<link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/linearicons/style.css') }}">
<!-- <link rel="stylesheet" href="{{ asset('vendor/chartist/css/chartist-custom.css') }}"> -->

<!-- wizard -->
<link rel="stylesheet" href="{{ asset('vendor/parsleyjs/css/parsley.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/sweetalert2/sweetalert2.css') }}">

<!-- select2 -->
<link rel="stylesheet" href="{{ asset('vendor/select2/css/select2.min.css') }}">

<!-- form input -->
<link rel="stylesheet" href="{{ asset('vendor/jquery-uii/themes/base/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('vendor/switchery/switchery.min.css') }}">

<!-- sweetalert css -->
<link rel="stylesheet" href="{{ asset('vendor/sweetalert/sweetalert.css') }}">

<!-- data table -->
<link rel="stylesheet" href="{{ asset('vendor/datatable/css/datatable.css') }}">

<!-- chart css -->
<!-- <link rel="stylesheet" href="{{ asset('vendor/chartist/css/chartist-custom.css') }}"> -->

<!-- main css -->
<link rel="stylesheet" href="{{ asset('css/main.css') }}">
<link rel="stylesheet" href="{{ asset('css/custom.css') }}">

<!-- FOR DEMO PURPOSES ONLY. You should remove this in your project -->
<link rel="stylesheet" href="{{ asset('css/demo.css') }}">

<!-- google fonts -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700" rel="stylesheet">

<!-- icons -->
<link rel="apple-touch-icon" sizes="76x76" href="{{ asset('img/favicon.png') }}">
<link rel="icon" type="image/png" sizes="96x96" href="{{ asset('img/favicon.png') }}">