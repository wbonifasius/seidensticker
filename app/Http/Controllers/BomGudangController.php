<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\BomGudangModel;

class BomGudangController extends Controller
{
   	//menampilkan data
    public function show()
    {
    	$bomgudangs = BomGudangModel::all();
    	return view('pages.admin.bom_gudang',compact('bomgudangs'));
    }
}
