<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\PibGudangModel;

class PibGudangController extends Controller
{
    //menampilkan data
    public function show()
    {
    	$pibgudangs = PibGudangModel::all();
    	return view('pages.admin.pib_gudang',compact('pibgudangs'));
    }
}
